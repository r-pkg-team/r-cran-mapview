Package: mapview
Type: Package
Title: Interactive Viewing of Spatial Data in R
Version: 2.5.0
Date: 2018-08-16
Authors@R: c(
      person("Tim", "Appelhans", email = "tim.appelhans@gmail.com", role = c("cre", "aut")),
      person("Florian", "Detsch", email = "fdetsch@web.de", role = c("aut")),
      person("Christoph", "Reudenbach", email = "reudenbach@geo.uni-marburg.de", role = c("aut")),
      person("Stefan", "Woellauer", email = "stephan.woellauer@geo.uni-marburg.de", role = c("aut")),
      person("Spaska", "Forteva", email = "spaska.forteva@geo.uni-marburg.de", role = c("ctb")),
      person("Thomas", "Nauss", email = "nauss@staff.uni-marburg.de", role = c("ctb")),
      person("Edzer", "Pebesma", role = c("ctb")),
      person("Kenton", "Russell", role = c("ctb")),
      person("Michael", "Sumner", role = c("ctb")),
      person("Jochen", "Darley", email = "Debugger@jedimasters.de", role = c("ctb")),
      person("Pierre", "Roudier", role = c("ctb")),
      person("Environmental Informatics Marburg", role = c("ctb"))
    )
Maintainer: Tim Appelhans <tim.appelhans@gmail.com>
Description: Quickly and conveniently create interactive visualisations of spatial data with or without background maps. Attributes of displayed features are fully queryable via pop-up windows. Additional functionality includes methods to visualise true- and false-color raster images, bounding boxes, small multiples and 3D raster data cubes. 
License: GPL (>= 3) | file LICENSE
URL: https://github.com/r-spatial/mapview
BugReports: https://github.com/r-spatial/mapview/issues
LazyData: TRUE
Depends: R (>= 2.10), methods
Imports: leaflet(>= 1.0.0), sp, raster, satellite, scales (>= 0.2.5),
        brew, htmlwidgets, htmltools, png, Rcpp (>= 0.11.3), lattice,
        webshot, viridisLite, sf, stars, base64enc, svglite, uuid
LinkingTo: Rcpp
RoxygenNote: 6.1.0
SystemRequirements: GNU make
ByteCompile: yes
Suggests: knitr, rmarkdown, dplyr, testthat, covr, lwgeom
NeedsCompilation: yes
Packaged: 2018-08-16 16:26:06 UTC; timpanse
Author: Tim Appelhans [cre, aut],
  Florian Detsch [aut],
  Christoph Reudenbach [aut],
  Stefan Woellauer [aut],
  Spaska Forteva [ctb],
  Thomas Nauss [ctb],
  Edzer Pebesma [ctb],
  Kenton Russell [ctb],
  Michael Sumner [ctb],
  Jochen Darley [ctb],
  Pierre Roudier [ctb],
  Environmental Informatics Marburg [ctb]
Repository: CRAN
Date/Publication: 2018-08-16 17:40:02 UTC
